const Router = require('express-promise-router')
const router = new Router();
const { insertCategory } = require('../controller/insertCategory')
const { deleteCategory } = require('../controller/deleteCategory')
const { getCategory } = require('../controller/getCategory')
const gard = require('../middleware/gard')
/* GET users listing. */
router.post('/insert',gard, insertCategory);
router.delete('/delete',gard, deleteCategory);
router.get('/get', getCategory);

module.exports = router;
