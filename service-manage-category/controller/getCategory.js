const db = require('../db')
async function getCategory(req, res) {
    try {
        var sql;
        var data;
        sql = `
        select product_category_id, product_category_name FROM product_category`
        data = await db.simpleExecute(sql, [])
        return res.status(200).send({
            result: true,
            payload:data
        })
    } catch (err) {
        if ((err.message).includes('PRIMARY')) {
            return res.status(401).send({
                result: false,
                message: 'Username is Ready'
            })
        } else {
            return res.status(500).send({
                result: false,
                message: err.message
            })
        }

    }
}

module.exports = {
    getCategory
}