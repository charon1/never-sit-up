const db = require('../db')
async function insertCategory(req, res) {
    var { category_name  } = req.body
    try {
        var sql;
        var data;
        sql = `
        insert into product_category(product_category_name, create_by, create_time)
        select ?,?,current_timestamp`
        data = await db.simpleExecute(sql, [category_name,req.user.user_id])
        return res.status(200).send({
            result: true,
            payload:data[0]
        })
    } catch (err) {
        if ((err.message).includes('PRIMARY')) {
            return res.status(401).send({
                result: false,
                message: 'Username is Ready'
            })
        } else {
            return res.status(500).send({
                result: false,
                message: err.message
            })
        }

    }
}

module.exports = {
    insertCategory
}