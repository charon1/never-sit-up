const db = require('../db')
async function deleteCategory(req, res) {
    var { category_id } = req.query
    try {
        Object.keys({ category_id }).map(key => {
            if (!req.query[key]) {
                return res.status(400).send({
                    result: false,
                    message: `Require ${key} for register`
                });
            }
        })

        var sql;
        var data;

        sql = `
        delete from product_category where product_category_id = ?;
`
        await db.simpleExecute(sql, [category_id])
        sql = `
        delete from product WHERE product_category_id = ?;
`
        await db.simpleExecute(sql, [category_id])
        res.status(200).send({
            result: true,
            message: "insert Basket Success"
        })
    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    deleteCategory
}
