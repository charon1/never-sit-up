const db = require('../db')
async function getProduct(req, res) {
    var { category } =req.query
    // console.log(username);
    try {
        var sql;
        var data;
        sql = `
        select pd.product_id,
                pd.product_name,
                pd.product_detail,
                pd.storck,
                pd.price,
                pd.create_time,
                pd.update_time,
                pd.create_by,
                pd.update_by,
                pd.unit,
                pc.product_category_name,
                pi.image_url
        FROM product p
        join product_detail pd on p.product_id = pd.product_id and pd.flag = 1
        left join product_category pc on p.product_category_id = pc.product_category_id
        join product_image pi on p.product_id = pi.product_id and ${"`index`"} = 1 and  pi.flag = 1

        `
        if (category){
            sql = sql + `
            where p.product_category_id = ?;
        `
        }
        console.log(sql);
        data = await db.simpleExecute(sql, [category])
        return res.status(200).send({
            result: true,
            payload:data
        })
    } catch (err) {
        if ((err.message).includes('PRIMARY')) {
            return res.status(401).send({
                result: false,
                message: 'Username is Ready'
            })
        } else {
            return res.status(500).send({
                result: false,
                message: err.message
            })
        }

    }
}

module.exports = {
    getProduct
}