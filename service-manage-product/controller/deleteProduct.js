const db = require('../db')
async function deleteProduct(req, res) {
    var { product_id } = req.query
    try {
        var sql;
        sql = `
        update product_detail
        set flag = 0,
        update_time = current_timestamp(),
        update_by = ?
        where product_id = ?`
        await db.simpleExecute(sql, [ req.user.user_id , product_id])
        sql = "update product_image set flag = 0 where product_id = ?"
        await db.simpleExecute(sql, [product_id])
        sql = "delete from product where product_id = ?"
        await db.simpleExecute(sql, [product_id])
        return res.status(200).send({
            result: true,
            message: "delete success"
        })
    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    deleteProduct
}