const db = require('../db')
async function updateProduct(req, res) {
    var { product_id,product_name,product_detail,price,unit ,image_url_list,storck,category_list} = req.body
    try {
        Object.keys({ product_id,product_name,product_detail,price,storck }).map(key => {
            if (!req.body[key]) {
                return res.status(400).send({
                    result: false,
                    message: `Require ${key} for register`
                });
            }
        })

        var sql;
        var data;
        var update = {
            product_name,product_detail,price,unit ,storck
        }
        sql = `
        update product_detail
        set  ?
        where product_id = ${product_id};`
        data = await db.simpleExecute(sql, [update])
        if(image_url_list.length > 0) {
            sql = "update product_image set flag = 0 where product_id = ?;"
            await db.simpleExecute(sql, [product_id])
            image_url_list.map(async (value,index) => {
                sql = "insert into product_image(product_id, `index`, image_url) SELECT ? ,? ,? ;"
                await db.simpleExecute(sql, [data.insertId,index + 1, value])
            })
        }
        if(category_list.length > 0) {
            sql = "delete from  product where product_id = ?"
            await db.simpleExecute(sql, [product_id])
            category_list.map(async (value) => {
                sql = "insert into product (product_id, product_category_id)   SELECT ? ,? ;"
                await db.simpleExecute(sql, [value,data.insertId])
            })
        }

        res.status(200).send({
            result: true,
            message: "insert Product Success"
        })
    } catch (err) {
        console.log(err);
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    updateProduct
}
