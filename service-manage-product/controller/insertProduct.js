const db = require('../db')
async function insertProduct(req, res) {
    var { product_name,product_detail,price,unit ,image_url_list,storck,category_list} = req.body
    try {
        Object.keys({ product_name,product_detail,price,storck }).map(key => {
            if (!req.body[key]) {
                return res.status(400).send({
                    result: false,
                    message: `Require ${key} for register`
                });
            }
        })

        var sql;
        var data;
        
        sql = `
        insert into product_detail ( product_name,product_detail, storck, price,unit, create_by, update_by) 
        SELECT ? ,? ,? ,? ,? ,? ,?`

        data = await db.simpleExecute(sql, [product_name,product_detail,storck, price,unit, req.user.user_id, req.user.user_id])
        if(image_url_list.length > 0) {
            image_url_list.map(async (value,index) => {
                sql = "insert into product_image(product_id, `index`, image_url,create_by,update_by) SELECT ? ,? ,? ;"
                await db.simpleExecute(sql, [data.insertId,index + 1, value,req.user.user_id,req.user.user_id])
            })
        }
        if(category_list.length > 0) {
            category_list.map(async (value) => {
                sql = "insert into product (product_id, product_category_id)   SELECT ? ,? ;"
                await db.simpleExecute(sql, [data.insertId,value])
            })
        }

        res.status(200).send({
            result: true,
            message: "insert Product Success"
        })
    } catch (err) {
        if ((err.message).includes('PRIMARY')) {
            return res.status(401).send({
                result: false,
                message: 'Username is Ready'
            })
        } else {
            return res.status(500).send({
                result: false,
                message: err.message
            })
        }

    }
}

module.exports = {
    insertProduct
}
