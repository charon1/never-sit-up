const Router = require('express-promise-router')
const router = new Router();
const { insertProduct } = require('../controller/insertProduct')
const { getProduct } = require('../controller/getProduct')
const { getProductDetail } = require('../controller/getProductDetail')
const { updateProduct } = require('../controller/updateProduct')
const { deleteProduct } = require('../controller/deleteProduct')
const gard = require('../middleware/gard')
/* GET users listing. */
router.post('/insert',gard, insertProduct);
router.patch('/update',gard, updateProduct);
router.delete('/delete',gard, deleteProduct);
router.get('/get', getProduct);
router.get('/getDetail', getProductDetail);

module.exports = router;
