const db = require('../db')
async function updateUser(req, res) {
    var { username } = req.body
    try {
        var sql;
        var data;
        sql = `
        update user_detail
        set ?,
        update_time = current_timestamp()
        where username = ?`
        data = await db.simpleExecute(sql, [req.body,username])
        return res.status(200).send({
            result: true,
            message: "update success"
        })
    } catch (err) {
        if ((err.message).includes('PRIMARY')) {
            return res.status(401).send({
                result: false,
                message: 'Username is Ready'
            })
        } else {
            return res.status(500).send({
                result: false,
                message: err.message
            })
        }

    }
}

module.exports = {
    updateUser
}