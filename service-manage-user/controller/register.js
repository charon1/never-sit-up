const db = require('../db')
async function Register(req, res) {
    var { username, password,first_name,last_name,phone_number, gender, image_url } = req.body
    try {
        Object.keys({ username, password, first_name,last_name, gender }).map(key => {
            if (!req.body[key]) {
                return res.status(400).send({
                    result: false,
                    message: `Require ${key} for register`
                });
            }
        })

        var sql;
        var data;
        password = require('crypto').createHash('sha256').update(password).digest('hex')
        sql = `
        insert into  user_auth (username, password, active, flag)
        SELECT ?, ?, ?, ?;`
        data = await db.simpleExecute(sql, [username, password, 1, 1])
        console.log(data);
        sql = `
        insert into  user_detail (username, first_name, last_name, gender)
        SELECT  ? , ? , ? , ? ;`
        data = await db.simpleExecute(sql, [username, first_name,last_name, gender])
        console.log(data);

        res.status(200).send({
            result: true,
            message: "regis success"
        })
    } catch (err) {
        if ((err.message).includes('PRIMARY')) {
            return res.status(401).send({
                result: false,
                message: 'Username is Ready'
            })
        } else {
            return res.status(500).send({
                result: false,
                message: err.message
            })
        }

    }
}

module.exports = {
    Register
}
