const db = require('../db')
async function getUserDetail(req, res) {
    var { username } =req.user
    console.log(username);
    try {
        var sql;
        var data;
        sql = `
        select *
        FROM user_detail
        where username = ?`
        data = await db.simpleExecute(sql, [username])
        return res.status(200).send({
            result: true,
            payload:data[0]
        })
    } catch (err) {
        if ((err.message).includes('PRIMARY')) {
            return res.status(401).send({
                result: false,
                message: 'Username is Ready'
            })
        } else {
            return res.status(500).send({
                result: false,
                message: err.message
            })
        }

    }
}

module.exports = {
    getUserDetail
}