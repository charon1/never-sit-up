const db = require('../db')
async function deleteUser(req, res) {
    var { username } = req.body
    try {
        var sql;
        sql = `
        update user_detail
        set flag = 0,
        update_time = current_timestamp()
        where username = ?;`
        await db.simpleExecute(sql, [ username])
        sql = `
        delete from user_auth
        where username = ?;`
        await db.simpleExecute(sql, [username])
        return res.status(200).send({
            result: true,
            message: "delete success"
        })
    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    deleteUser
}