const Router = require('express-promise-router')
const router = new Router();
const { updateUser } = require('../controller/updateUser')
const { Register } = require('../controller/register')
const { deleteUser } = require('../controller/deleteUser')
const { getUserDetail } = require('../controller/getUserDetail')
const gard = require('../middleware/gard')
/* GET users listing. */
router.patch('/update',gard, updateUser);
router.post('/register', Register);
router.delete('/delete',gard, deleteUser);
router.get('/detail',gard, getUserDetail);

module.exports = router;
