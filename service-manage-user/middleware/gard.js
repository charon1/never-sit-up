const request = require('request')

module.exports = async function (req, res, next) {
    // // console.log('Request URL:', req.originalUrl)
    // next()
    try {
        apiPath = `http://service-auth:8001/api/v1/auth/verify`
        // console.log(req.headers.authorization);

        await request.get(apiPath, {
            auth: {
                'bearer': req.headers.authorization.split(' ')[1]
            }
        }, async (error, reso, body) => {
            // // console.log(body);

            if (error) {
                return res.status(500).send(error)
            }
            if (reso.statusCode > 299) {
                return res.status(reso.statusCode).send(body)
            } else {
                req.user = JSON.parse(body).payload
                next()
            }
        })
    } catch (error) {
        // console.log('error ==> Guard');
        
        return res.status(401).send({result:false,message:'Err =>  Request Token Header'})
    }

}
