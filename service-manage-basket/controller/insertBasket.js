const db = require('../db')
async function insertBasket(req, res) {
    var { product_id,num_of_product,price} = req.body
    try {
        Object.keys({ product_id,num_of_product,price }).map(key => {
            if (!req.body[key]) {
                return res.status(400).send({
                    result: false,
                    message: `Require ${key} for register`
                });
            }
        })

        var sql;
        var data;
        sql = `
        insert into basket(user_id, product_id, num_of_product, price) 
        SELECT ?,?,?,?`
        data = await db.simpleExecute(sql, [req.user.user_id,product_id,num_of_product,price])

        res.status(200).send({
            result: true,
            message: "insert Basket Success"
        })
    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    insertBasket
}
