const db = require('../db')
async function updateBasket(req, res) {
    var { basket_id,num_of_product,price} = req.body
    try {
        Object.keys({ basket_id,num_of_product,price }).map(key => {
            if (!req.body[key]) {
                return res.status(400).send({
                    result: false,
                    message: `Require ${key} for register`
                });
            }
        })

        var sql;
        var data;
        var update = {
            num_of_product,price 
        }
        sql = `
        update basket
        set ?,
        update_time = current_timestamp
        where basket_id = ?`
        data = await db.simpleExecute(sql, [update,basket_id])

        res.status(200).send({
            result: true,
            message: "update Basket Success"
        })
    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    updateBasket
}
