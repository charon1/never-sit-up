const db = require('../db')
async function deleteBasket(req, res) {
    var { basket_id } = req.query
    try {
        var sql;
        sql = `
        update basket
        set flag = 0,
        update_time = current_timestamp()
        where basket_id = ?`
        await db.simpleExecute(sql, [ basket_id])
        return res.status(200).send({
            result: true,
            message: "delete success"
        })
    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    deleteBasket
}