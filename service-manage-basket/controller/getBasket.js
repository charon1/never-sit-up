const db = require('../db')
async function getBasket(req, res) {
    try {
        var sql;
        var data;
        sql = `
        select pd.product_id,
            pd.product_name,
            pd.product_detail,
            pd.storck,
            pd.price,
            pd.unit,
            b.price,
            b.num_of_product,
            b.basket_id,
            pi.image_url
        FROM basket b
        join product_detail pd on b.product_id = pd.product_id and pd.flag = 1
        left join product_image pi on b.product_id = pi.product_id and ${"`index`"} = 1
        where b.flag = 1 and b.purchase = 0 and user_id = ?;


        `
        data = await db.simpleExecute(sql, [req.user.user_id])
        return res.status(200).send({
            result: true,
            payload:data
        })
    } catch (err) {
        if ((err.message).includes('PRIMARY')) {
            return res.status(401).send({
                result: false,
                message: 'Username is Ready'
            })
        } else {
            return res.status(500).send({
                result: false,
                message: err.message
            })
        }

    }
}

module.exports = {
    getBasket
}