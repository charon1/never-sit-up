const Router = require('express-promise-router')
const router = new Router();
const { insertBasket } = require('../controller/insertBasket')
const { getBasket } = require('../controller/getBasket')
const { updateBasket } = require('../controller/updateBasket')
const { deleteBasket } = require('../controller/deleteBasket')
const gard = require('../middleware/gard')
/* GET users listing. */
router.post('/insert',gard, insertBasket);
router.patch('/update',gard, updateBasket);
router.delete('/delete',gard, deleteBasket);
router.get('/get',gard, getBasket);

module.exports = router;
