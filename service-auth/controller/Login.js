const db = require('../db')
const { jwtConfig } = require('../config');
const jwt = require('jsonwebtoken');
async function Login(req, res) {
    try {
        var username;
        var password;
        var signPayload;
        var sql;

        if (req.headers.authorization) {
            hearders = req.headers.authorization.split(' ')
            // let data = 
            buff = new Buffer.from(hearders[1], 'base64');
            let text = buff.toString('utf-8').split(':')
            username = text[0]
            password = require('crypto').createHash('sha256').update(text[1]).digest('hex')

        } else {
            res.set('WWW-Authenticate', 'Basic realm="401"')
            return res.status(401).send({
                result: false,
                code: 401,
                message: "Login Use basic auth"
            })
        }
        console.log(password);
        sql = `SELECT ua.username,first_name as firstName, last_name as lastName, gender,phone_number
                FROM user_auth ua
                join user_detail ud 
                where ua.username = ? and  ua.flag = 1 and password = ?`
        var data = await db.simpleExecute(sql, [username,password])
        
        sql = `update user_auth 
                set active = 1,
                    update_time = current_timestamp()
                where username = ?`
        await db.simpleExecute(sql, [username])
        if(data.length > 0){
            signPayload = {
                userInfo: {
                    ...data[0]
                }
            };
            const accessToken = jwt.sign(
                signPayload,
                jwtConfig.accessTokenPrivateKEY,
                {
                    algorithm: 'RS256',
                    expiresIn: jwtConfig.accessTokenExpiresIn
                }
            );
            return res.status(200).send({
                result: true,
                token: accessToken,
                payload:data[0]
            })
        }else{
            return res.status(401).send({
                result: false,
                message: 'username or password incorract'
            })
        }
        
    } catch (err) {

        res.status(500).send({
            result: false,
            // code: 500,
            message: err.message
        })
    }
}

module.exports = {
    Login
}