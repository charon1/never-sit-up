const db = require('../db')
async function Logout(req, res) {
    try {
        var sql;
        var data;
        sql = `
        update user_auth 
        set active = 0,
            update_time = current_timestamp()
        where username = ?`
        await db.simpleExecute(sql, [req.user.username])
        sql = `
        insert into  token_expired (token)
        SELECT ?;`
        await db.simpleExecute(sql, [req.token])
        return res.status(200).send({
            result: true,
            message: "Logout"
        })
    } catch (err) {
        if ((err.message).includes('PRIMARY')) {
            return res.status(401).send({
                result: false,
                message: 'Username is Ready'
            })
        } else {
            return res.status(500).send({
                result: false,
                message: err.message
            })
        }

    }
}

module.exports = {
    Logout
}