const db = require('../db')
async function VerifyToken(req, res) {
    try {
        var sql;
        var data;
        
        sql = `
        select *
        from token_expired
        where token = ?`
        data = await db.simpleExecute(sql, [req.token])
        if(data.length > 0){
            return res.status(401).send({
                result: false,
                message: 'Token expired'
            })
        }else{
            sql = `
            update user_auth 
            set active = 1,
            update_time = current_timestamp()
            where username = ?`
            await db.simpleExecute(sql, [req.user.username])
            sql = `
            select * 
            FROM user_detail
            where username = ? and flag = 1`
            data = await db.simpleExecute(sql, [req.user.username])
            return res.status(200).send({
                result: true,
                payload: data[0],
            })
        }
        
    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    VerifyToken
}