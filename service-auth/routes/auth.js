const Router = require('express-promise-router')
const router = new Router();
const { Login } = require('../controller/Login')
const { Logout } = require('../controller/Logout')
const { VerifyToken } = require('../controller/VerifyToken')
const gard = require('../middleware/gard')
/* GET users listing. */
router.get('/login', Login);
router.get('/logout',gard, Logout);
router.get('/verify',gard, VerifyToken);

module.exports = router;
