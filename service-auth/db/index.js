const sql = require("mysql");
const config = require('../config');

const pool = sql.createPool(config.mysqlConfig);

async function initialize() {
     pool.getConnection((err, connection) => {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('db -> Database has too many connections.')
            }
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('db -> Database has too many connections.')
            }
            if (err.code === 'ECONNREFUSED') {
                console.error('db -> Database connection was refused.')
            }
            // return res.status(500).send(err)

        }else{
            console.log('connect Database success');

            if(connection){

                connection.release()
            }
            // return next()


        }
        return
        
    });
}



async function simpleExecute(sql, values = []) {
    try {
        return new Promise((resolve, reject) => {
            pool.getConnection((err, connection) => {
                if (err) {
                    console.error('err : simpleExecute getConnection ->' + err)
                    reject(err);
                } else {
                    connection.beginTransaction(function (err) {
                        if (err) {
                            console.log(err.message);
                            reject(err)
                            // throw err;
                        }
                        connection.query(sql, values, function (error, results, fields) {
                            if (error) {
                                return connection.rollback(function () {
                                    console.log(error.message);
                                    reject(error)
                                });
                            }
                            connection.commit(function (err) {
                                if (err) {
                                    return connection.rollback(function () {
                                        reject(err)
                                        // throw err;
                                    });
                                }
                                connection.release()
                                resolve(results)
                                // console.log('success!');
                            });

                        });
                    });
                }
            })
        });
    } catch (err) {
        resolve(err)
        console.error('error : simpleExecute2 -> ' + err)

    }
}



async function dbClose() {
    pool.end()
}

module.exports = { initialize, simpleExecute, dbClose }
