const Router = require('express-promise-router')
const router = new Router();
const { insertOrder } = require('../controller/insertOrder')
const { getOrder } = require('../controller/getOrder')
const { deleteOrder } = require('../controller/deleteOrder')
const gard = require('../middleware/gard')
/* GET users listing. */
router.post('/insert',gard, insertOrder);
router.delete('/delete',gard, deleteOrder);
router.get('/get',gard, getOrder);

module.exports = router;
