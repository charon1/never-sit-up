const Router = require('express-promise-router')
const router = new Router();
const { getOrder } = require('../controller/getOrderHistory')
const gard = require('../middleware/gard')
/* GET users listing. */
router.get('/',gard, getOrder);

module.exports = router;
