var express = require('express');
var router = express.Router();
var user = require('./user')
var history = require('./history')
/* GET home page. */

module.exports = app => {
    app.use('/api/v1/order',user)
    app.use('/api/v1/order/history',history)
    
};
