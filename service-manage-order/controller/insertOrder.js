const db = require('../db')
async function insertOrder(req, res) {
    var { basket_id } = req.body
    try {
        Object.keys({ basket_id }).map(key => {
            if (!req.body[key]) {
                return res.status(400).send({
                    result: false,
                    message: `Require ${key} for register`
                });
            }
        })

        var sql;
        var data;
        sql = `
        update basket
        set purchase = 1,
            update_time = current_timestamp
        where  basket_id = ? and user_id = ?`
        await db.simpleExecute(sql, [basket_id,req.user.user_id])

        sql = `
        insert into ${"`order`"}( user_id, product_id, num_of_product, price) 
        SELECT user_id,product_id,num_of_product,price
        FROM basket
        WHERE basket_id = ? and user_id = ?`
        await db.simpleExecute(sql, [basket_id,req.user.user_id])

        res.status(200).send({
            result: true,
            message: "insert Basket Success"
        })
    } catch (err) {
        console.log(err);

        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    insertOrder
}
