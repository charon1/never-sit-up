const db = require('../db')
async function deleteOrder(req, res) {
    var { order_id } = req.query
    try {
        Object.keys({ order_id }).map(key => {
            if (!req.query[key]) {
                return res.status(400).send({
                    result: false,
                    message: `Require ${key} for register`
                });
            }
        })

        var sql;
        var data;

        sql = `
        update ${"`order`"}
        set cancal = 1,
            update_time = current_timestamp
        where order_id = ?`
        await db.simpleExecute(sql, [order_id])

        res.status(200).send({
            result: true,
            message: "insert Basket Success"
        })
    } catch (err) {
        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    deleteOrder
}
