const db = require('../db')
async function getOrder(req, res) {
    try {
        var sql;
        var data;
        sql = `
        select pd.product_id,
            pd.product_name,
            pd.product_detail,
            pd.storck,
            pd.price,
            pd.unit,
            o.price,
            o.num_of_product,
            o.order_id,
            pi.image_url
        FROM ${"`order`"} o
        join product_detail pd on o.product_id = pd.product_id and pd.flag = 1
        left join product_image pi on o.product_id = pi.product_id and ${"`index`"} = 1
        where o.cancal = 0 and o.success = 0 and o.user_id = ?;
        `
        data = await db.simpleExecute(sql, [req.user.user_id])
        return res.status(200).send({
            result: true,
            payload:data
        })
    } catch (err) {
        console.log(err);

        return res.status(500).send({
            result: false,
            message: err.message
        })

    }
}

module.exports = {
    getOrder
}