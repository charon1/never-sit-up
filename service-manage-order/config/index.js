
module.exports = {
  "mysqlConfig": {
    // connectionLimit: 200, // Just to curb our enthusiasm.
    host: process.env.MYSQL_HOST || '35.213.137.81',
    user: process.env.MYSQL_USER || 'root',
    password: process.env.MYSQL_PASS || 'dos@database',
    port: process.env.MYSQL_PORT || 3306,
    database: process.env.MYSQL_DATABASE_NAME || 'seversitup',
    multipleStatements: true,
    insecureAuth : true
  }

}
