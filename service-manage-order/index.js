
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
var logger = require('morgan');
const routes = require('./routes');
const helmet = require('helmet')
const db = require('./db')
// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');

var app = express();
db.initialize()
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(helmet())
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
routes(app);

const APP_PORT = process.env.PORT || 8004;
try {
    console.log('Initializing DataBase');
    db.initialize()
} catch (err) {
    console.error(err);
    process.exit(1);
}
try {
    console.log('Initializing web service');
    server = app.listen(APP_PORT, () =>
        console.log('SNN  Service Listening on port ' + APP_PORT)
    );
} catch (err) {
    console.error(err);
    process.exit(1);
}
